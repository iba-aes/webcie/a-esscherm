//This is the main javascript file for the new TV screen. This file handles most of the setup and timings


//Arrays to store the activities and AMO posters
var acts = [];
var extraPosters = [];

var lastUpdated;
var activiteitenUrl = "https://www.a-eskwadraat.nl/Ajax/Activiteiten/tv_posters2.json"
var hefInfoUrl = "https://lustrum.a-eskwadraat.nl/acthook.php"
var websiteUrl = "https://www.a-eskwadraat.nl"

//Retrieve the URL paramters
var url = new URL(window.location.href);
var aantalActs = url.searchParams.get("aantalActs");
var timePerAct = url.searchParams.get("timePerAct");
var actUpdateTime = url.searchParams.get("actUpdateTime");
var localTest = url.searchParams.get("localTest");
var aantalExtraPosters = url.searchParams.get("aantalExtraPosters");
var actsPerExtraPoster = url.searchParams.get("actsPerExtraPoster");
var extraPostersPerCycle = url.searchParams.get("extraPostersPerCycle");

//Default values
const defaultAantalActs = 5; 
const defaultTimePerAct = 10000; 
const defaultActUpdateTime = 600000; 
const defaultAantalExtraPosters = 3; 
const defaultExtraPostersPerCycle = 2; 
const fallbackPoster = "default.png"

//HTML tags used
const posterId = "container--poster"
const actId = "container--events"
const highlightId = "container--highlight"
const extraPosterId = "";
const activeClass = "active";
const blurClass = "blur"


//Parse the url parameters
if (aantalActs == null)
	aantalActs = defaultAantalActs;
if (timePerAct == null)
	timePerAct = defaultTimePerAct;
if (actUpdateTime == null)
	actUpdateTime = defaultActUpdateTime;
if (aantalExtraPosters == null)
	aantalExtraPosters = defaultAantalExtraPosters;
if (actsPerExtraPoster == null)
	actsPerExtraPoster = aantalActs;
if (extraPostersPerCycle == null)
	extraPostersPerCycle = defaultExtraPostersPerCycle;
if (localTest) {
	hefInfoUrl = "test.txt"
}

//Update Activities with callback
function updateActiviteiten(call) {
	lastUpdated = new Date();
	console.log("Updating Activities");
	
	$.get(activiteitenUrl, function(data) {
		acts = []
		data.activiteiten.forEach(function(actData) {
			var act = new activiteit(actData);
			acts.push(act);
		});

		aantalActs = Math.min(defaultAantalActs, acts.length);

		if (actsPerExtraPoster > aantalActs){
			actsPerExtraPoster = aantalActs;
		}

		//Callback
		call()
	});
}

function updateExtraPosters(call) {
	console.log("Updating Extra Posters");
	
	$.get(activiteitenUrl, function(data) {
		extraPosters = data['extra-posters'];

		aantalExtraPosters = Math.min(defaultAantalExtraPosters, extraPosters.length);
		extraPostersPerCycle = Math.min(defaultExtraPostersPerCycle, aantalExtraPosters);

		//Callback
		call()
	});

	//If actsPerExtraPoster was not set using parameters set it to once per all activities
	if (!actsPerExtraPoster) {
		actsPerExtraPoster = aantalActs;
	}
}


//Download file from given uri
function downloadURI(uri, name) {
	var link = document.createElement("a");
	link.download = name;
	link.href = uri;
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
	delete link;
}

//Create an element with given text
function createel(text, el) {
	var a = document.createElement(el);
	a.textContent = text;
	return a;
}

function emptyimgcont() {
	var imgCont = document.getElementById(posterId);
	imgCont.innerHTML = "";
}

//Filling the act div with activities
function fillacts() {

	//Remove the current acts
	var cont = document.getElementById(actId);
	var imgCont = document.getElementById(posterId);
	cont.innerHTML = "";

	//Load the activities
	for (var i = 0; i < aantalActs; i++) {

		//Add the act and act poster
		var div = acts[i].maakactdiv("act" + i);
		var img = acts[i].maakimg("img" + i);
		img.classList.add("poster");

		img.onerror = function () {
			//The image was not an valid image, change to default poster
			this.src = fallbackPoster;
		};

		cont.appendChild(div);
		imgCont.appendChild(img);
		if (i == currentAct) {
			img.hidden = false;
			div.classList.add(activeClass);
			timeSinceLastExtraPoster = 1;
		}
	}
	updating = false;
}

function fillextraposters() {

	//Remove the current extra posters
	var imgCont = document.getElementById(posterId);

	//Load the posters
	for (var i = 0; i < aantalExtraPosters; i++) {

		//Add the act and act poster
		var img = maakposterimg(extraPosters[i], i)
		img.classList.add("poster");

		img.onerror = function () {
			//The image was not an valid image, change to default poster
			this.src = fallbackPoster;
		};

		imgCont.appendChild(img);
	}
}

function maakposterimg(data, index) {
	var img = document.createElement("img")
	img.src = websiteUrl + data.pad
	img.id = "extraPoster" + index
	img.hidden = true;
	return img
}

var aantalacts = 0
var updating = false;
var timeLastUpdated = 0;

function updateAll() {
	if (Date.now() - timeLastUpdated > actUpdateTime) {
		updating = true;
		timeLastUpdated = Date.now();
		resetCycleVariables();
		emptyimgcont();
		updateActiviteiten(function() {
			fillacts();
		});
		updateExtraPosters(function() {
			fillextraposters();
		});
	}

}

//Starts the changing of acts and other posters
function startSlideShow() {


	var timer = setInterval(changeAct, timePerAct);
	//var updateTimeing = setInterval(updateAll,actUpdateTime);
}

var currentAct = 0
var currentExtraPoster = 0;
var timeSinceLastExtraPoster = 0;
var sequentialExtraPosterCount = 0;

function resetCycleVariables() {
	currentAct = 0;
	currentExtraPoster = 0;
	timeSinceLastExtraPoster = 0;
	sequentialExtraPosterCount = 0;
}

function hideCurrentImmages() {
	//Get the current immages
	var img = document.getElementById("img" + currentAct)
	var imgextraPoster = document.getElementById("extraPoster" + currentExtraPoster)
	var div = document.getElementById("act" + currentAct)

	//Remove active from activeClass
	div.classList.remove(activeClass)

	//Hide them
	if (img != undefined)
		img.hidden = true;
	if (imgextraPoster != undefined)
		imgextraPoster.hidden = true;

}
//Creates the highlight popup and blur
function changeVisual(blur) {
	var actCont = document.getElementById(actId)
	var highlightCont = document.getElementById(highlightId);
	if (!blur) {
		actCont.style.filter = ""
		actCont.style.webkitFilter = ""

		highlightCont.style.opacity = "";
		highlightCont.style.height = "";
	} else {
		//actCont.style.filter = "url(data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' ><filter id='svgMask'><feGaussianBlur stdDeviation='8' /></filter></svg>#svgMask);"
		actCont.style.filter = "blur(8px)"
		//actCont.style.webkitFilter = "blur(8px);  -moz-filter: blur(8px); -o-filter: blur(8px);  -ms-filter: blur(8px)"
		actCont
		highlightCont.style.opacity = 1;
		highlightCont.style.height = "12em";
	}
}

function nextAMO() {
	currentExtraPoster++;
	sequentialExtraPosterCount++;
	//var div = document.getElementById(actId)

	var img = document.getElementById("extraPoster" + currentExtraPoster)
	if (img == undefined) {
		currentExtraPoster = 0
		img = document.getElementById("extraPoster" + currentExtraPoster)
	}
	changeVisual(false);
	img.hidden = false;
}

function nextAct() {
	currentAct++;
	var img = document.getElementById("img" + currentAct)

	if (img == undefined) {
		//We had all the acts, we change back to the first one
		currentAct = 0
		img = document.getElementById("img" + currentAct)
	}
	try {
		var div = document.getElementById("act" + currentAct)
		div.classList.add(activeClass)
		changeVisual(false);
		img.hidden = false;
		timeSinceLastExtraPoster++;
		sequentialExtraPosterCount = 0;
	}
	catch (error) {
		console.log("No acts")
	}
}

function changeAct() {

	updateAll();

	if (updating)
		return

	if (sequentialExtraPosterCount == extraPostersPerCycle) {
		timeSinceLastExtraPoster = 0;
	}

	if ((sequentialExtraPosterCount > extraPostersPerCycle || timeSinceLastExtraPoster < actsPerExtraPoster) && aantalActs > 0 || extraPosters.length == 0) {
		hideCurrentImmages();
		nextAct();
	}
	else {
		//We change the activvity to an AMO poster.
		hideCurrentImmages();
		nextAMO();
	}
}

function GetBannerInfo() {
	$.getJSON(hefInfoUrl, function (data) {
		var date = document.getElementById('HEF-date')
		var name = document.getElementById('HEF-name')
		var time = document.getElementById('HEF-time')
		var timer = document.getElementById('HEF-timer')

		date.innerHTML = data.date
		name.innerHTML = data.name
		time.innerHTML = data.time
		timer.innerHTML = data.timer
	});
}

var timer;
$(function () {
	//Get and clear the needed containers
	var cont = document.getElementById(actId);
	var imgCont = document.getElementById(posterId);
	cont.innerHTML = "";
	imgCont.innerHTML = "";

	//Add the items
	updateAll();

	//Start the changing
	startSlideShow()
	console.log("starting")

})
