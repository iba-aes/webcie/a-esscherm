//Used to create date strings
var dagen = ["Zo","Ma","Di","Wo","Do","Vr","Za"]
var maanden = ["jan","feb","mrt","apr","mei","jun","jul","aug","sept","okt","nov","dec"]

var websiteUrl = "https://www.a-eskwadraat.nl"

class activiteit{

	constructor(data){
		this.naam = data.naam;
		this.start = new Date(data.momentBegin);
		this.end = new Date(data.momentEind);
		this.locatie = data.locatie;
		this.posterurl = websiteUrl + data.pad;
	}

	getDateStartString(){
		return dagen[this.start.getDay()] + " " + this.start.getDate() + " " + maanden[this.start.getMonth()];
	}
	//Returns the time string for an activiteit
	getTimeStartString(){
		var d = new Date();
		var h = this.start.getHours().toString()
		if(h.length ==1){
			h = 0+h
		}
		var m = this.start.getMinutes().toString();
		if(m.length ==1){
			m = 0+m
		}
		return h + ":"+m
	}

	maakactdiv(id){
	var div = document.createElement("div");
	div.id = id;
	div.classList.add("event");
	var maindiv = document.createElement("div");
	maindiv.classList.add("event--main");
	var datediv = document.createElement("div");
	datediv.classList.add("event--date");

	maindiv.appendChild(createel(this.naam,"h1"))
	maindiv.appendChild(createel(this.locatie,"p"))
	datediv.appendChild(createel(this.getDateStartString(),"h3"))
	datediv.appendChild(createel("","span"))
	datediv.appendChild(createel(this.getTimeStartString(),"h3"))
	div.appendChild(maindiv)
	div.appendChild(datediv)
	return div
	}
	maakimg(id){
		var img = document.createElement("img")
		img.src = this.posterurl
		img.id = id
		img.hidden = true;
		return img
	}

}
